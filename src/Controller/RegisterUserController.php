<?php

namespace Coderaf\Spiderman\SecurityBundle\Controller;

use Coderaf\Spiderman\SecurityBundle\Exception\MalformedTokenException;
use Coderaf\Spiderman\SecurityBundle\Service;
use Coderaf\Spiderman\SecurityBundle\User;
use Kreait\Firebase\JWT\Contract\Token;
use Kreait\Firebase\JWT\IdTokenVerifier;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterUserController
{
    private $serializer;
    private $validator;
    private $service;
    private $idTokenVerifier;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        Service $service,
        IdTokenVerifier $idTokenVerifier
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->service = $service;
        $this->idTokenVerifier = $idTokenVerifier;
    }

    /**
     * Remember to unsecure this path, it secure itself, checking new access token
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
       $this->authorizeRequest($request);

        /** @var User $user */
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            return new JsonResponse(
              ['message' => (string) $errors],
              Response::HTTP_BAD_REQUEST
            );
        }
        $response = $this->service->postUser($user);

        return new JsonResponse([], $response->getStatusCode());
    }

    private function authorizeRequest(Request $request)
    {
        $accessToken = $request->headers->get('Access-Token');
        $token = $this->idTokenVerifier->verifyIdTokenWithLeeway($accessToken, 300);
        $this->validateToken($token);
    }

    private function validateToken(Token $token): void
    {
        if (
          !is_array($token->payload())
          || !isset($token->payload()['email'])
          || !isset($token->payload()['user_id'])
        ) {
            throw new MalformedTokenException('Token missing mandatory fields.');
        }
    }
}
