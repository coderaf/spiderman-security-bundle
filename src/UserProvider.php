<?php

namespace Coderaf\Spiderman\SecurityBundle;

use Coderaf\Spiderman\SecurityBundle\Exception\MalformedTokenException;
use Kreait\Firebase\JWT\Contract\Token;
use Kreait\Firebase\JWT\IdTokenVerifier;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $spidermanService;
    private $idTokenVerifier;

    public function __construct(
        Service $spidermanService,
        IdTokenVerifier $idTokenVerifier
    ) {
        $this->spidermanService = $spidermanService;
        $this->idTokenVerifier = $idTokenVerifier;
    }

    public function refreshUser(UserInterface $user)
    {
        throw new \Exception('Not implemented. Should not be reached.');
    }

    public function supportsClass(string $class)
    {
        return $class === User::class;
    }

    public function loadUserByUsername(string $accessToken): UserInterface
    {
        try {
            $token = $this->idTokenVerifier->verifyIdTokenWithLeeway($accessToken, 300);
            $this->validateToken($token);

            $response = $this->spidermanService->getUser(['externalId' => $token->payload()['user_id']]);
            $decoded = json_decode((string) $response->getBody());
            $this->validateResponse($decoded);

            return $this->denormalize($decoded);
        } catch (\Exception $e) {
            throw new UsernameNotFoundException('Could not find user.', 0, $e);
        }
    }

    private function denormalize($decoded): User
    {
        $user = new User();
        $user->setId($decoded->id);
        $user->setEmail($decoded->email);
        $user->setExternalId($decoded->externalId);

        return $user;
    }

    private function validateToken(Token $token): void
    {
        if (
            !is_array($token->payload())
            || !isset($token->payload()['email'])
            || !isset($token->payload()['user_id'])
        ) {
            throw new MalformedTokenException('Token missing mandatory fields.');
        }
    }

    private function validateResponse($decodedResponse): void
    {
        if (
            !isset($decodedResponse->id)
            || !isset($decodedResponse->email)
        ) {
            throw new UsernameNotFoundException('Could not find user.');
        }
    }
}
