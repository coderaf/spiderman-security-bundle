<?php

namespace Coderaf\Spiderman\SecurityBundle;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /**
     *  we store email here at moment
     */
    private $username;
    private $id;
    private $externalId;
    private $email;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getRoles()
    {
        return ["ROLE_USER"];
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    /*
     * as we use firebase to store players, we use email instead of username, but symfony userInterace require username
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        return null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }
}
