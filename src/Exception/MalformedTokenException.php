<?php

namespace Coderaf\Spiderman\SecurityBundle\Exception;

class MalformedTokenException extends \Exception
{
}
