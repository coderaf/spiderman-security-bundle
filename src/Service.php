<?php

namespace Coderaf\Spiderman\SecurityBundle;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\SerializerInterface;

class Service
{
    private $httpClient;
    private $normalizer;

    public function __construct(Client $httpClient, SerializerInterface $normalizer)
    {
        $this->httpClient = $httpClient;
        $this->normalizer = $normalizer;
    }

    public function getUser(array $data): ResponseInterface
    {
        $url = sprintf('api/user/%s', $data['externalId']);

        return $this->httpClient->request('GET', $url);
    }

    public function postUser(User $user): ResponseInterface
    {
        $data = $this->normalizer->normalize($user, 'array');
        unset($data['id']);

        $url = 'api/user';
        return $this->httpClient->request(
            'POST',
            $url,
            [
                RequestOptions::JSON => $data
            ]
        );
    }
}
