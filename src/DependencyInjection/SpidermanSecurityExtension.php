<?php

namespace Coderaf\Spiderman\SecurityBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SpidermanSecurityExtension extends Extension
{
    public function getAlias()
    {
        return 'spiderman_security';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('spiderman.firebase_project_id', $config['firebase_project_id']);
        $container->setParameter('spiderman.base_uri', $config['base_uri']);
        $container->setParameter('spiderman.api_key', $config['api_key']);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(
                __DIR__ . '/../Resources/config'
            )
        );
        $loader->load('services.yaml');
    }
}
