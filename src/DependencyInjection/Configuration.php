<?php

namespace Coderaf\Spiderman\SecurityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('spiderman_security');

        $builder->getRootNode()
            ->children()
                ->scalarNode('firebase_project_id')->end()
                ->scalarNode('base_uri')->end()
                ->scalarNode('api_key')->end()
            ->end()
        ;
        return $builder;
    }
}
