<?php

namespace Coderaf\Spiderman\SecurityBundle;

use Coderaf\Spiderman\SecurityBundle\DependencyInjection\SpidermanSecurityExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SpidermanSecurityBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new SpidermanSecurityExtension();
    }
}
