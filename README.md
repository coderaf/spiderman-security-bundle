# Spiderman Security Bundle

### Instalation

As its private repository it requires special care in composer.json

in composer.json add

```
{
    ..
    "require": {
        "coderaf/spiderman-security-bundle": "0.2.9"
    },
    "repositories": [
        {
            "name": "coderaf/spiderman-security-bundle",
            "type": "vcs",
            "url": "git@bitbucket.org-coderaf:coderaf/spiderman-security-bundle.git"
        }
    ],
} 
```
Also add configuration file in  
```
    config/packages/spiderman_security.yaml
```
with 
```
spiderman_security:
    base_uri: '%env(SPIDERMAN_BASE_URI)%'
    api_key: '%env(SPIDERMAN_API_KEY)%'
    firebase_project_id: '%env(FIREBASE_AUTH_PROJECT_ID)%'
```

then in `.env` file add proper parameters
```
SPIDERMAN_BASE_URI
SPIDERMAN_API_KEY
FIREBASE_AUTH_PROJECT_ID
```